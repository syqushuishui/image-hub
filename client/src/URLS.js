export default {
  landing: '/',
  login: '/login',
  register: '/register',
  newImage: '/new-image',
  main: '/image',
  search: '/search',
  profile: '/profile',
  result: '/result'
};
