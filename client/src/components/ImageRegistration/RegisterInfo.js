import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
// Material UI components
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';
// Icons
// Project components
import AppAppBar from '../../components/Navigation/TopAppBar/AppAppBar';
import ImageService from '../../services/ImageService';
import URLS from '../../URLS';

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit * 3,
    [theme.breakpoints.down('sm')]: {
      display: 'flex',
      flexDirection: 'column'
    }
  },
  submit: {
    display: 'flex',
    marginTop: theme.spacing.unit * 8,
    margin: 'auto'
  },
  clear: {
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing.unit * 2
    }
  },
  img: {
    imageOrientation: 'from-image',
    width: '100%'
  },
  date: {
    borderRadius: '5px',
    border: '1px solid lightgray',
    height: '35px',
    width: '225px'
  }
});

class RegisterInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      isSucessful: false,
      url: '',
      date: new Date(),
      tags: '',
      latitude: null,
      longitude: null,
      errorMessage: '',
      orientation: 1
    };
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(date) {
    this.setState({
      date: date
    });
  }
  // From degrees, minutes, seconds to decimal degrees
  dmsTodeg(degrees, minutes, seconds) {
    return (degrees + minutes / 60 + seconds / 3600).toFixed(4);
  }
  getData = () => {
    ImageService.getMetadata()
      .then(res => {
        this.setState({
          date: this.formatDatetime(res.data.exif.DateTimeOriginal),
          latitude: this.dmsTodeg(
            res.data.gps.GPSLatitude[0],
            res.data.gps.GPSLatitude[1],
            res.data.gps.GPSLatitude[2]
          ),
          longitude: this.dmsTodeg(
            res.data.gps.GPSLongitude[0],
            res.data.gps.GPSLongitude[1],
            res.data.gps.GPSLongitude[2]
          )
        });
      })
      .catch(err => {
        console.log(err.message);
      });
  };

  getTags = () => {
    ImageService.getTags(localStorage.getItem('url'))
      .then(res => {
        this.setState({
          tags:
            JSON.parse(res.data.body).result.tags[0].tag.en +
            ' ' +
            JSON.parse(res.data.body).result.tags[1].tag.en +
            ' ' +
            JSON.parse(res.data.body).result.tags[2].tag.en
        });
      })
      .catch(err => {
        console.log(err.message);
      });
  };

  clear = () => {
    this.setState({
      isWithMetadata: false,
      latitude: null,
      longitude: null,
      date: new Date(),
      tags: ''
    });
  };
  formatDatetime = str => {
    var array = str.split(/:| /);
    return new Date(
      array[0],
      array[1] - 1,
      array[2],
      array[3],
      array[4],
      array[5]
    );
  };

  componentDidMount() {
    this.setState({
      url: localStorage.getItem('url')
    });
    //localStorage.removeItem('url');
    ImageService.downloadImage(localStorage.getItem('url'))
      .then(res => {
        ImageService.getMetadata()
          .then(res => {
            this.setState({
              orientation: res.data.image.Orientation
            });
          })
          .catch(err => {
            console.log(err.message);
          });
      })
      .catch(err => {
        console.log(err.message);
      });
  }

  submit = event => {
    event.preventDefault();
    console.log(this.state.date);
    ImageService.registerImage(
      this.state.url,
      this.state.date,
      this.state.tags,
      this.state.latitude,
      this.state.longitude,
      this.state.orientation,
      localStorage.getItem('userId')
    )
      .then(res => {
        this.props.history.push(URLS.main);
      })
      .catch(err => {
        this.setState({
          errorMessage: err.response.data.message,
          isLoading: false
        });
      });
  };
  switchOrientation(orientation) {
    switch (orientation) {
      case 3:
        return (
          <img
            src={this.state.url}
            className={this.props.classes.img}
            alt='lalala'
            width='550'
            height='300'
            style={{ transform: 'rotate(180deg)' }}
          />
        );
      case 6:
        return (
          <img
            className={this.props.classes.img}
            src={this.state.url}
            alt='lalala'
            width='270'
            height='240'
            style={{ transform: 'rotate(90deg)' }}
          />
        );
      default:
        return (
          <img
            src={this.state.url}
            className={this.props.classes.img}
            alt='lalala'
            width='550'
            height='300'
          />
        );
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <main className={classes.main}>
        <AppAppBar />
        <Paper className={classes.paper}>
          {this.switchOrientation(this.state.orientation)}

          <form className={classes.form} onSubmit={this.submit}>
            <Button variant='contained' onClick={this.getData}>
              Use metadata
            </Button>
            <Button
              className={classes.clear}
              variant='contained'
              onClick={this.getTags}
            >
              Get three auto-tags
            </Button>
            <Button
              className={classes.clear}
              variant='contained'
              onClick={this.clear}
            >
              Clear
            </Button>
            <br />
            <br />
            <InputLabel htmlFor='date'>Date *</InputLabel>
            <br />

            <DatePicker
              className={classes.date}
              selected={this.state.date}
              onChange={this.handleChange}
              todayButton={'Today'}
              showTimeSelect
              dateFormat='yyyy/MM/dd, HH:mm'
              timeIntervals={15}
            />

            <FormControl margin='normal' required fullWidth>
              <InputLabel htmlFor='tags'>Tags</InputLabel>
              <Input
                id='tags'
                name='tags'
                value={this.state.tags}
                onChange={event => {
                  this.setState({ tags: event.target.value });
                }}
              />
            </FormControl>
            <FormControl margin='normal' required fullWidth>
              <InputLabel htmlFor='latitude'>Latitude</InputLabel>
              <Input
                id='latitude'
                name='latitude'
                onChange={event => {
                  this.setState({ latitude: event.target.value });
                }}
                value={this.state.latitude || ''}
              />
            </FormControl>
            <FormControl margin='normal' required fullWidth>
              <InputLabel htmlFor='longitude'>Longitude</InputLabel>
              <Input
                id='longitude'
                onChange={event => {
                  this.setState({ longitude: event.target.value });
                }}
                value={this.state.longitude || ''}
              />
            </FormControl>
            <Typography variant='caption' color='error' gutterBottom>
              {this.state.errorMessage}
            </Typography>
            <Button
              type='submit'
              variant='contained'
              color='primary'
              className={classes.submit}
            >
              Submit
            </Button>
          </form>
        </Paper>
      </main>
    );
  }
}

export default withStyles(styles)(RegisterInfo);
