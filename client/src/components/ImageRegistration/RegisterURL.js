import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
// Material UI components
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
// Icons
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternateOutlined';
// Project components
import AppAppBar from '../../components/Navigation/TopAppBar/AppAppBar';
import { TextField } from '@material-ui/core';

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit * 3
  },
  submit: {
    display: 'flex',
    marginTop: theme.spacing.unit * 8,
    margin: 'auto'
  }
});

class RegisterURL extends Component {
  state = {
    url: ''
  };

  submit = () => {
    localStorage.setItem('url', this.state.url);
    this.props.history.push('new-image/info');
  };
  render() {
    const { classes } = this.props;
    return (
      <main className={classes.main}>
        <AppAppBar />
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <AddPhotoAlternateIcon />
          </Avatar>

          <Typography component='h1' variant='subtitile1'>
            Register new image
          </Typography>
          <form className={classes.form} onSubmit={this.submit}>
            <FormControl margin='normal' required fullWidth>
              <TextField
                id='url'
                label='URL'
                variant='outlined'
                autoComplete='url'
                onChange={event => {
                  this.setState({ url: event.target.value });
                }}
              />
            </FormControl>
            <Button
              type='submit'
              variant='contained'
              color='primary'
              className={classes.submit}
            >
              Continue
            </Button>
          </form>
        </Paper>
      </main>
    );
  }
}

export default withStyles(styles)(RegisterURL);
