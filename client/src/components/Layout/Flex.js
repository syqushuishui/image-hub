import * as React from 'react';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';

const styles = {
  root: {
    display: 'flex'
  }
};

const Flex = props => {
  const { classes } = props;
  const flexClass = {
    ...classes,
    justifyContent: props.justify || 'flex-start',
    alignItems: props.align || 'center',
    flexDirection: props.dir || 'row'
  };

  return (
    <div
      {...props}
      style={flexClass}
      className={classNames(classes.root, props.className)}
    >
      {props.children}
    </div>
  );
};

export default withStyles(styles)(Flex);
