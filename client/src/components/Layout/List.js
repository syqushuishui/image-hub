import * as React from 'react';
import { withStyles } from '@material-ui/core/styles';
import classNames from 'classnames';

// Project Components
import Flex from './Flex';

const styles = {
  item: {
    padding: 12,
    width: '100%'
  }
};

const List = props => {
  return (
    <Flex className={props.className} dir='column'>
      {props.children}
    </Flex>
  );
};

export default List;

export const ListItem = withStyles(styles)(props => {
  const { classes } = props;
  return (
    <div {...props} className={classNames(classes.item, props.className)}>
      {props.children}
    </div>
  );
});
