import React, { Component } from 'react';
// Material UI components
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
// Icons
import WarningIcon from '@material-ui/icons/Warning';
// Project components
import AppAppBar from '../Navigation/TopAppBar/AppAppBar';
import UserService from '../../services/UserService';
import MessageDialog from './MessageDialog';
import Progress from './Progress';

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 3
  },
  title: {
    marginBottom: theme.spacing.unit * 3
  }
});

class ForgotPassword extends Component {
  state = {
    isLoading: false,
    showDialog: false,
    email: '',
    errorMessage: ''
  };

  resetPassword = event => {
    event.preventDefault();
    this.setState({ isLoading: true });
    UserService.resetPassword(this.state.email)
      .then(res => {
        this.setState({
          showDialog: true,
          isLoading: false
        });
      })
      .catch(err => {
        this.setState({
          errorMessage: err,
          isLoading: false
        });
      });
  };
  render() {
    const { classes } = this.props;
    return (
      <div>
        <main className={classes.main}>
          <AppAppBar />
          <CssBaseline />
          <Paper className={classes.paper}>
            {this.state.isLoading ? (
              <Progress />
            ) : (
              <React.Fragment>
                <Avatar className={classes.avatar}>
                  <WarningIcon />
                </Avatar>
                <Typography
                  className={classes.title}
                  component='h1'
                  variant='subtitile1'
                >
                  Forgot Your Password?
                </Typography>
                <Typography component='h1' variant='body2'>
                  Enter your email address below and we'll send you a link to
                  reset your password.
                </Typography>
                <form className={classes.form} onSubmit={this.resetPassword}>
                  <FormControl margin='normal' required fullWidth>
                    <InputLabel htmlFor='email'>Email Address</InputLabel>
                    <Input
                      id='email'
                      name='email'
                      autoComplete='email'
                      autoFocus
                      onChange={event => {
                        this.setState({ email: event.target.value });
                      }}
                    />
                  </FormControl>
                  <Button
                    type='submit'
                    fullWidth
                    variant='contained'
                    color='primary'
                    className={classes.submit}
                  >
                    Submit
                  </Button>
                </form>
                <Typography variant='caption' color='error' gutterBottom>
                  {this.state.errorMessage}
                </Typography>
              </React.Fragment>
            )}
          </Paper>
        </main>
        <MessageDialog
          open={this.state.showDialog}
          onClose={() => this.setState({ showDialog: false })}
          title='Please check your email'
          content='We have sent you a email with the link to reset your password.'
        />
      </div>
    );
  }
}

export default withStyles(styles)(ForgotPassword);
