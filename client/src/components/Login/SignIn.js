import React, { Component } from 'react';
// Material UI components
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import Link from '@material-ui/core/Link';
// Icons
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
// Project components
import AppAppBar from '../Navigation/TopAppBar/AppAppBar';
import UserService from '../../services/UserService';
import Progress from './Progress';
import URLS from '../../URLS';

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 2
  }
});

class SignIn extends Component {
  state = {
    isLoading: false,
    isSucessful: false,
    email: '',
    password: '',
    errorMessage: ''
  };
  loginHandler = event => {
    event.preventDefault();
    this.setState({ isLoading: true });
    UserService.loginUser(this.state.email, this.state.password)
      .then(res => {
        localStorage.setItem('authToken', res.data.token);
        localStorage.setItem('userId', res.data.userId);
        this.props.history.push(URLS.main);
      })
      .catch(err => {
        this.setState({
          errorMessage: err.response.data.message,
          isLoading: false
        });
      });
  };
  render() {
    const { classes } = this.props;
    return (
      <main className={classes.main}>
        <AppAppBar />
        <CssBaseline />
        <Paper className={classes.paper}>
          {this.state.isLoading ? (
            <Progress />
          ) : (
            <React.Fragment>
              <Avatar className={classes.avatar}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component='h1' variant='subtitile1'>
                Sign In
              </Typography>
              <form className={classes.form} onSubmit={this.loginHandler}>
                <FormControl margin='normal' required fullWidth>
                  <InputLabel htmlFor='email'>Email Address</InputLabel>
                  <Input
                    id='email'
                    name='email'
                    autoComplete='email'
                    autoFocus
                    onChange={event => {
                      this.setState({ email: event.target.value });
                    }}
                  />
                </FormControl>
                <FormControl margin='normal' required fullWidth>
                  <InputLabel htmlFor='password'>Password</InputLabel>
                  <Input
                    name='password'
                    type='password'
                    id='password'
                    autoComplete='current-password'
                    onChange={event => {
                      this.setState({ password: event.target.value });
                    }}
                  />
                </FormControl>
                <Typography variant='caption' color='error' gutterBottom>
                  {this.state.errorMessage}
                </Typography>
                <Button
                  type='submit'
                  fullWidth
                  variant='contained'
                  color='primary'
                  className={classes.submit}
                >
                  Sign in
                </Button>
              </form>
              <Typography align='center' marginTop='10px'>
                <Link underline='always' href='/forgot-password'>
                  Forgot password?
                </Link>
              </Typography>
            </React.Fragment>
          )}
        </Paper>
      </main>
    );
  }
}

export default withStyles(styles)(SignIn);
