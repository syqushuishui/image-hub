import React, { Component } from 'react';
// Material UI components
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import CircularProgress from '@material-ui/core/CircularProgress';
// Icons
import PersonAddOutlinedIcon from '@material-ui/icons/PersonAddOutlined';
// Project components
import AppAppBar from '../Navigation/TopAppBar/AppAppBar';
import UserService from '../../services/UserService';
import Success from '../../assets/success.png';

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    marginBottom: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 3
  },
  successButton: {
    marginTop: theme.spacing.unit * 3
  },
  img: {
    width: '100%',
    maxWidth: 450,
    height: 'auto',
    objectFit: 'cover',
    display: 'block',
    margin: 'auto'
  },
  imageWrapper: {
    display: 'block',
    margin: 'auto',
    '@media only screen and (max-width: 600px)': {
      maxWidth: 275
    }
  }
});

class SignUp extends Component {
  state = {
    isLoading: false,
    isSucessful: false,
    username: '',
    email: '',
    password: '',
    confirmedPassword: '',
    errorMessage: ''
  };
  componentDidMount() {}
  // For creating a new account
  createUser = event => {
    event.preventDefault();
    // Validate input
    if (this.state.password === this.state.confirmedPassword) {
      if (this.state.email.includes('@')) {
        // Creating new account
        this.setState({ isLoading: true });
        UserService.createUser(
          this.state.username,
          this.state.email,
          this.state.password
        )
          .then(res => {
            this.setState({
              isSucessful: true,
              isLoading: false
            });
          })
          .catch(err => {
            this.setState({
              errorMessage: err.response.data.message,
              isLoading: false
            });
          });
      } else {
        this.setState({
          errorMessage: 'Your email address is not valid'
        });
      }
    } else {
      this.setState({
        errorMessage: 'Password and confirm password does not match'
      });
    }
  };

  render() {
    const { classes } = this.props;
    if (this.state.isLoading) {
      return (
        <main className={classes.main}>
          <AppAppBar />
          <CssBaseline />
          <Paper className={classes.paper}>
            <CircularProgress />
          </Paper>
        </main>
      );
    } else if (this.state.isSucessful) {
      return (
        <main className={classes.main}>
          <AppAppBar />
          <CssBaseline />
          <Paper className={classes.paper}>
            <Typography component='h1' variant='subtitile1' gutterBottom>
              User was created
            </Typography>
            <div className={classes.imageWrapper}>
              <img className={classes.img} src={Success} alt='success' />
            </div>
            <Button
              className={classes.successButton}
              variant='contained'
              color='primary'
              href='/login'
            >
              Log in now!
            </Button>
          </Paper>
        </main>
      );
    }
    return (
      <main className={classes.main}>
        <AppAppBar />
        <CssBaseline />
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <PersonAddOutlinedIcon />
          </Avatar>
          <Typography component='h1' variant='subtitile1'>
            Register
          </Typography>
          <form className={classes.form} onSubmit={this.createUser}>
            <FormControl margin='normal' required fullWidth>
              <InputLabel htmlFor='username'>Username</InputLabel>
              <Input
                id='username'
                name='username'
                autoComplete='username'
                autoFocus
                onChange={event => {
                  this.setState({ username: event.target.value });
                }}
              />
            </FormControl>
            <FormControl margin='normal' required fullWidth>
              <InputLabel htmlFor='email'>Email Address</InputLabel>
              <Input
                id='email'
                name='email'
                autoComplete='email'
                autoFocus
                onChange={event => {
                  this.setState({ email: event.target.value });
                }}
              />
            </FormControl>
            <FormControl margin='normal' required fullWidth>
              <InputLabel htmlFor='password'>Password</InputLabel>
              <Input
                name='password'
                type='password'
                id='password'
                autoComplete='current-password'
                onChange={event => {
                  this.setState({ password: event.target.value });
                }}
              />
            </FormControl>{' '}
            <FormControl margin='normal' required fullWidth>
              <InputLabel htmlFor='confirm'>Confirm Password</InputLabel>
              <Input
                id='confirm'
                name='confirm'
                type='password'
                onChange={event => {
                  this.setState({ confirmedPassword: event.target.value });
                }}
              />
            </FormControl>
            <Typography variant='caption' color='error' gutterBottom>
              {this.state.errorMessage}
            </Typography>
            <Button
              type='submit'
              fullWidth
              variant='contained'
              color='primary'
              className={classes.submit}
            >
              Sign up
            </Button>
          </form>
        </Paper>
      </main>
    );
  }
}
export default withStyles(styles)(SignUp);
