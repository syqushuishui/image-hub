import React, { Fragment, Component } from 'react';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
// External components
import Lightbox from 'react-image-lightbox';
import 'react-image-lightbox/style.css';

const styles = () => ({
  root: {
    cursor: 'pointer'
  },
  o3: {
    transform: 'rotate(180deg)'
  },
  o6: {
    transform: 'rotate(90deg)'
  }
});

class ClickableImage extends Component {
  state = {
    open: false
  };

  switchOrientation(orientation, image) {
    switch (orientation) {
      case 3:
        return (
          <Lightbox
            mainSrc={image}
            wrapperClassName={this.props.classes.o3}
            onCloseRequest={() => {
              this.setState({ open: false });
              this.props.click(false);
            }}
          />
        );

      case 6:
        return (
          <Lightbox
            mainSrc={image}
            wrapperClassName={this.props.classes.o6}
            onCloseRequest={() => {
              this.setState({ open: false });
              this.props.click(false);
            }}
          />
        );

      default:
        return (
          <Lightbox
            mainSrc={image}
            onCloseRequest={() => {
              this.setState({ open: false });
              this.props.click(false);
            }}
          />
        );
    }
  }

  render() {
    const { classes, className, image, alt, orientation, ...rest } = this.props;
    return (
      <Fragment>
        <img
          onClick={() => {
            this.setState({ open: !this.state.open });
            this.props.click(true);
          }}
          className={classNames(classes.root, className)}
          src={image}
          alt={alt}
          {...rest}
        />
        {this.state.open && this.switchOrientation(orientation, image)}
      </Fragment>
    );
  }
}

export default withStyles(styles)(ClickableImage);
