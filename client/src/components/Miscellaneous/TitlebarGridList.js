import React, { Component } from 'react';
import dateFormat from 'dateformat';
// Material UI components
import { withStyles } from '@material-ui/core/styles';
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';
import { compose } from 'recompose';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper
  },
  gridList: {
    width: 1000,
    height: '100%',
    '@media only screen and (max-width: 600px)': {
      width: '100%'
    }
  },
  icon: {
    color: 'rgba(255, 255, 255, 0.54)'
  },
  item: {
    alignItems: 'center'
  }
});

class TitlebarGridList extends Component {
  getGridListCols = () => {
    if (isWidthUp('sm', this.props.width)) {
      return 4;
    }
    return 2;
  };
  switchOrientation(orientation, tile) {
    switch (orientation) {
      case 3:
        return (
          <img
            src={tile.url}
            alt={tile.title}
            style={{
              transform: 'rotate(180deg) translate(150px)'
            }}
          />
        );
      case 6:
        return (
          <img
            src={tile.url}
            alt={tile.title}
            style={{
              transform: 'rotate(90deg) translate(-20px, 225px) ',
              backgroundSize: 'cover',
              height: '260px'
            }}
          />
        );
      default:
        return <img src={tile.url} alt={tile.title} />;
    }
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <GridList
          cellHeight={180}
          cols={this.getGridListCols()}
          className={classes.gridList}
        >
          <GridListTile key='Subheader' cols={4} style={{ height: 'auto' }}>
            <ListSubheader component='div'>{this.props.title}</ListSubheader>
          </GridListTile>
          {this.props.tileData.map(tile => (
            <GridListTile key={tile.id} className={classes.item} cols={1}>
              {this.switchOrientation(tile.orientation, tile)}
              <GridListTileBar
                title={dateFormat(tile.date, 'isoDate')}
                subtitle={<span>{tile.tags}</span>}
                actionIcon={
                  <IconButton
                    className={classes.icon}
                    onClick={() => {
                      window.location.href = '/image/' + tile.id;
                    }}
                  >
                    <InfoIcon />
                  </IconButton>
                }
              />
            </GridListTile>
          ))}
        </GridList>
      </div>
    );
  }
}

export default compose(
  withWidth(),
  withStyles(styles)
)(TitlebarGridList);
