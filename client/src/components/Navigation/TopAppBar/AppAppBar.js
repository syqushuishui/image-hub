import React, { Component, Fragment } from 'react';
// Material UI components
import { withStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import Button from '@material-ui/core/Button';
import Avatar from '@material-ui/core/Avatar';
import Drawer from '@material-ui/core/Drawer';
import IconButton from '@material-ui/core/IconButton';
import Hidden from '@material-ui/core/Hidden';
// Icons
import Menu from '@material-ui/icons/Menu';
// Project components
import Flex from '../../Layout/Flex';
import AppBar from './AppBar';
import Sidebar from './Sidebar';
import Toolbar, { styles as toolbarStyles } from './Toolbar';
import UserDefault from '../../../assets/User.jpg';
import UserService from '../../../services/UserService';
import URLS from '../../../URLS';

const styles = theme => ({
  title: {
    fontSize: 26
  },
  placeholder: toolbarStyles(theme).root,
  toolbar: {
    justifyContent: 'space-between'
  },
  left: {
    flex: 'initial'
  },
  leftLinkActive: {
    color: theme.palette.common.white
  },
  right: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-end',
    flexDirection: 'row'
  },
  rightLink: {
    fontSize: 16,
    color: theme.palette.common.white,
    marginLeft: theme.spacing.unit * 3,
    textAlign: 'center',
    '@media only screen and (max-width: 600px)': {
      display: 'none'
    }
  },
  avatar: {
    // border: '2px solid white ',
    marginLeft: theme.spacing.unit * 3,
    cursor: 'pointer',
    '@media only screen and (max-width: 600px)': {
      display: 'none'
    }
  },
  outlineBtn: {
    border: '2px solid',
    marginLeft: 10
  },
  uriBtn: {
    color: 'white',
    '@media only screen and (max-width: 900px)': {
      fontSize: 8
    }
  },
  appBar: {
    zIndex: 24
  },
  drawer: {
    position: 'relative',
    zIndex: 1,
    paddingTop: '56px !important'
  }
});
const RightMenu = withStyles(styles)(props => (
  <Flex className={props.classes.white}>
    <Hidden mdUp>
      <IconButton color='inherit' onClick={props.onMenuClick}>
        <Menu />
      </IconButton>
    </Hidden>
    {UserService.isAuthenticated() ? (
      <Fragment>
        <Button
          color='inherit'
          underline='none'
          className={props.classes.rightLink}
          onClick={props.goToMyImages}
        >
          {'My Images'}
        </Button>
        <Button
          color='inherit'
          underline='none'
          className={props.classes.rightLink}
          onClick={props.imageSearch}
        >
          {'Search'}
        </Button>
        <Button
          color='inherit'
          underline='none'
          className={props.classes.rightLink}
          onClick={props.imageRegister}
        >
          {'Register a photo'}
        </Button>
        <Button
          color='inherit'
          underline='none'
          className={props.classes.rightLink}
          onClick={props.logOut}
        >
          {'Log out'}
        </Button>
        <Avatar
          className={props.classes.avatar}
          onClick={props.goToProfile}
          src={UserDefault}
        />
      </Fragment>
    ) : (
      <Hidden smDown>
        <Button
          underline='none'
          className={props.classes.rightLink}
          href='/register'
        >
          {'Sign Up'}
        </Button>
        <Button
          className={props.classes.outlineBtn}
          href='/login'
          variant='outlined'
          color='inherit'
        >
          {'Log in'}
        </Button>
      </Hidden>
    )}
  </Flex>
));

class AppAppBar extends Component {
  state = {
    isAuth: false,
    showMenu: false,
    toggleMenu: false
  };
  logOut = () => {
    UserService.logOut();
    window.location.href = URLS.landing;
  };
  imageRegister = () => {
    window.location.href = URLS.newImage;
  };
  imageSearch = () => {
    window.location.href = URLS.search;
  };
  goToProfile = () => {
    window.location.href = URLS.profile;
  };
  goToMyImages = () => {
    window.location.href = URLS.main;
  };
  toggleMenu = () => {
    this.setState({ showMenu: !this.state.showMenu });
  };
  goTo = page => {
    this.toggleMenu();
    window.location.href = page;
  };

  render() {
    const { classes } = this.props;
    return (
      <Fragment>
        <AppBar position='absolute' className={classes.appBar}>
          <Toolbar className={classes.toolbar}>
            <div className={classes.left} />
            <Link
              variant='h6'
              underline='none'
              color='inherit'
              className={classes.title}
              href='/'
            >
              {'image hub'}
            </Link>

            <div className={classes.right}>
              <RightMenu
                goTo={page => this.goTo(page)}
                onMenuClick={this.toggleMenu}
                goToMyImages={this.goToMyImages}
                goToProfile={this.goToProfile}
                imageSearch={this.imageSearch}
                imageRegister={this.imageRegister}
                logOut={this.logOut}
              />
            </div>
          </Toolbar>
        </AppBar>
        <Hidden mdUp>
          <Drawer
            anchor='top'
            open={this.state.showMenu}
            onClose={this.toggleMenu}
            className={classes.drawer}
          >
            <Sidebar
              goTo={this.goTo}
              isAuthorized={UserService.isAuthenticated()}
            />
          </Drawer>
        </Hidden>

        <div className={classes.placeholder} />
      </Fragment>
    );
  }
}
export default withStyles(styles)(AppAppBar);
