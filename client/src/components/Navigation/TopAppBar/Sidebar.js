import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import URLS from '../../../URLS';
import classNames from 'classnames';
// Material UI Components
import Typography from '@material-ui/core/Typography';
import Icon from '@material-ui/core/Icon';
// Icons
import Add from '@material-ui/icons/Add';
import Person from '@material-ui/icons/Person';
import Search from '@material-ui/icons/Search';
import Images from '@material-ui/icons/PhotoLibrary';
import PersonAdd from '@material-ui/icons/PersonAdd';
import Lock from '@material-ui/icons/Lock';
// Project Components
import List from '../../Layout/List';
import Flex from '../../Layout/Flex';

const styles = theme => ({
  root: {
    zIndex: 1600,
    paddingTop: 56,
    '@media only screen and (min-width: 600px)': {
      paddingTop: 64
    }
  },
  urlBtn: {
    minHeight: 58,
    backgroundColor: theme.palette.secondary.main,
    color: 'white',
    width: '100%',
    borderBottom: '1px solid rgba(0,0,0,0.1)'
  },
  grid: {
    width: '100%',
    display: 'grid',
    gridTemplateColumns: '1fr 1fr'
  },
  small: {
    minHeight: 48
  },
  text: {
    marginLeft: 10
  }
});

const URLButton = withStyles(styles)(props => {
  const { classes } = props;
  return (
    <Flex
      className={classNames(classes.urlBtn, props.small ? classes.small : '')}
      onClick={() => props.goTo(props.url)}
      justify='center'
    >
      {props.icon && <Icon>{props.icon}</Icon>}
      <Typography
        className={classes.text}
        variant='title'
        color='inherit'
        align='center'
        cursor='point'
      >
        {props.label}
      </Typography>
    </Flex>
  );
});

const Sidebar = props => {
  const { classes } = props;
  return (
    <List className={classes.root}>
      {props.isAuthorized ? (
        <React.Fragment>
          <URLButton
            url={URLS.main}
            label='My images'
            goTo={props.goTo}
            icon={<Images />}
          />
          <URLButton
            url={URLS.newImage}
            label='Register new image'
            goTo={props.goTo}
            icon={<Add />}
          />
          <URLButton
            url={URLS.search}
            label='Search'
            goTo={props.goTo}
            icon={<Search />}
          />
          <URLButton
            url={URLS.profile}
            label='Profile'
            goTo={props.goTo}
            icon={<Person />}
          />
        </React.Fragment>
      ) : (
        <React.Fragment>
          <URLButton
            url={URLS.login}
            label='login'
            goTo={props.goTo}
            icon={<Lock />}
          />
          <URLButton
            url={URLS.register}
            label='register'
            goTo={props.goTo}
            icon={<PersonAdd />}
          />
        </React.Fragment>
      )}
    </List>
  );
};

export default withStyles(styles)(Sidebar);
