import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';

// Material UI components

// Icons

// Project components

const styles = {
  root: {}
};

class Template extends Component {
  render() {
    const { classes } = this.props;
    return <div className={classes.root} />;
  }
}

export default withStyles(styles)(Template);
