import React, { Component } from 'react';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
// Project components
import theme from '../theme';
import SignIn from '../components/Login/SignIn';
import SignUp from '../components/Login/SignUp';
import ForgotPassword from '../components/Login/ForgotPassword';
import ResetPassword from '../components/Login/ResetPassword';
import Frontpage from './views/Frontpage';
import RegisterURL from '../components/ImageRegistration/RegisterURL';
import RegisterInfo from '../components/ImageRegistration/RegisterInfo';
import decode from 'jwt-decode';
import Mainpage from './views/Mainpage';
import ImageDetails from './views/Mainpage/Components/ImageDetails';
import LocationDetails from './views/Mainpage/Components/LocationDetails';
import Search from './views/Search';
import Profile from './views/Profile';
import Resultpage from './views/Resultpage';

const checkAuth = () => {
  const token = localStorage.getItem('authToken');
  if (token === 'undefined') {
    return false;
  }
  try {
    const { exp } = decode(token); //seconds
    if (exp < new Date().getTime() / 1000) {
      // getTime method is the number of milliseconds
      return false;
    }
  } catch (e) {
    return false;
  }
  return true;
};

const AuthRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      checkAuth() ? (
        <Component {...props} />
      ) : (
        <Redirect to={{ pathname: '/login' }} />
      )
    }
  />
);

class App extends Component {
  render() {
    return (
      <div className='App'>
        <BrowserRouter>
          <MuiThemeProvider theme={theme}>
            <Switch>
              <Route exact path='/' component={Frontpage} />
              <Route exact path='/login' component={SignIn} />
              <Route exact path='/register' component={SignUp} />
              <Route exact path='/forgot-password' component={ForgotPassword} />
              <Route exact path='/reset-password' component={ResetPassword} />
              <AuthRoute exact path='/new-image' component={RegisterURL} />
              <AuthRoute
                exact
                path='/new-image/info'
                component={RegisterInfo}
              />
              <AuthRoute exact path='/image' component={Mainpage} />
              <AuthRoute exact path='/image/:id' component={ImageDetails} />
              <AuthRoute
                exact
                path='/image/:id/map'
                component={LocationDetails}
              />
              <AuthRoute exact path='/search' component={Search} />
              <AuthRoute exact path='/profile' component={Profile} />
              <AuthRoute exact path='/result' component={Resultpage} />
            </Switch>
          </MuiThemeProvider>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
