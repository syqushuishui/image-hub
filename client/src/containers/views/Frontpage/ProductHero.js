import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Layout from '../../../components/Layout/Layout';
import UserService from '../../../services/UserService';

// Display four pictures randomly
var pic = [];
pic[0] =
  'https://images.unsplash.com/photo-1548407260-da850faa41e3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1487&q=80';
pic[1] =
  'https://images.unsplash.com/photo-1546663481-614a764c0404?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1500&q=80';
pic[2] =
  'https://images.unsplash.com/photo-1547844149-792ce502416a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80';
pic[3] =
  'https://images.unsplash.com/photo-1546989288-bee08331e8aa?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=751&q=80';

var randomIndex = Math.round(Math.random() * 3);
var backgroundImage = pic[randomIndex];

const styles = theme => ({
  background: {
    backgroundImage: `url(${backgroundImage})`,
    backgroundColor: '#7fc7d9', // Average color of the background image.
    backgroundPosition: 'center'
  },
  button: {
    minWidth: 200
  },
  h5: {
    marginBottom: theme.spacing.unit * 4,
    marginTop: theme.spacing.unit * 4,
    [theme.breakpoints.up('sm')]: {
      marginTop: theme.spacing.unit * 10
    },
    [theme.breakpoints.down('sm')]: {
      padding: theme.spacing.unit * 4
    }
  },
  moreText: {
    marginTop: theme.spacing.unit * 1
  }
});

function ProductHero(props) {
  const { classes } = props;

  return (
    <Layout backgroundClassName={classes.background}>
      {/* Increase the network loading priority of the background image. */}
      <img style={{ display: 'none' }} src={backgroundImage} alt='' />
      <Typography color='inherit' align='center' variant='h2' marked='center'>
        Hub for your images
      </Typography>
      <Typography
        color='inherit'
        align='center'
        variant='h5'
        className={classes.h5}
      >
        Easy to manage your personal photos from different online stores.
      </Typography>
      {UserService.isAuthenticated() ? (
        <Button
          color='secondary'
          variant='contained'
          size='large'
          className={classes.button}
          href='/image'
        >
          My Images
        </Button>
      ) : (
        <Button
          color='secondary'
          variant='contained'
          size='large'
          className={classes.button}
          href='/register'
        >
          Register
        </Button>
      )}
    </Layout>
  );
}

ProductHero.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ProductHero);
