import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import LayoutBody from '../../../components/Layout/LayoutBody';
import Typography from '@material-ui/core/Typography';
//Icons
import AddPhotoAlternateIcon from '@material-ui/icons/AddPhotoAlternateOutlined';
import ImageSearchIcon from '@material-ui/icons/ImageSearchOutlined';
import SatelliteIcon from '@material-ui/icons/SatelliteOutlined';

const styles = theme => ({
  root: {
    display: 'flex',
    overflow: 'hidden',
    backgroundColor: theme.palette.secondary.light
  },
  layoutBody: {
    marginTop: theme.spacing.unit * 15,
    marginBottom: theme.spacing.unit * 30,
    display: 'flex',
    position: 'relative'
  },
  item: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `0px ${theme.spacing.unit * 5}px`
  },
  image: {
    height: 55
  },
  title: {
    marginTop: theme.spacing.unit * 5,
    marginBottom: theme.spacing.unit * 5
  }
});

function ProductValues(props) {
  const { classes } = props;

  return (
    <section className={classes.root}>
      <LayoutBody className={classes.layoutBody} width='large'>
        <Grid container spacing={40}>
          <Grid item xs={12} md={4}>
            <div className={classes.item}>
              <AddPhotoAlternateIcon />
              <Typography variant='h6' className={classes.title}>
                Image registration
              </Typography>
              <Typography variant='h5'>
                {
                  'By registering, You can register all your images from different online store with their URLs.'
                }
                {''}
              </Typography>
            </div>
          </Grid>
          <Grid item xs={12} md={4}>
            <div className={classes.item}>
              <ImageSearchIcon />
              <Typography variant='h6' className={classes.title}>
                Image search
              </Typography>
              <Typography variant='h5'>
                {
                  'Get an overview of all the images registered, and find the ones you want to see.'
                }
                {''}
              </Typography>
            </div>
          </Grid>
          <Grid item xs={12} md={4}>
            <div className={classes.item}>
              <SatelliteIcon />
              <Typography variant='h6' className={classes.title}>
                Map
              </Typography>
              <Typography variant='h5'>
                {
                  'We provided a map service for you to find the images directly on the map. '
                }
                {''}
              </Typography>
            </div>
          </Grid>
        </Grid>
      </LayoutBody>
    </section>
  );
}

ProductValues.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ProductValues);
