import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import ProductHero from './ProductHero';
import ProductValues from './ProductValues';

// Material UI components

// Icons

// Project components

const styles = {
  root: {}
};

class Frontpage extends Component {
  render() {
    const { classes } = this.props;
    return (
      <main className={classes.main}>
        <ProductHero />
        <ProductValues />
      </main>
    );
  }
}

export default withStyles(styles)(Frontpage);
