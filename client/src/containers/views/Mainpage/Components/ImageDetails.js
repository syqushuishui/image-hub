import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import dateFormat from 'dateformat';
// Material UI components
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import PlaceIcon from '@material-ui/icons/Place';
// Icons
// Project components
import AppAppBar from '../../../../components/Navigation/TopAppBar/AppAppBar';
import ImageService from '../../../../services/ImageService';
import ClickableImage from '../../../../components/Miscellaneous/ClickableImage';

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit * 3
  },
  return: {
    display: 'flex',
    marginTop: theme.spacing.unit * 8,
    margin: 'auto'
  }
});
class ImageDetails extends Component {
  state = {
    image: [],
    open: false
  };
  handleOpen = isOpen => {
    this.setState({
      open: isOpen
    });
  };
  goToMap = () => {
    this.props.history.push(this.props.match.url + '/map');
  };
  return = () => {
    this.props.history.goBack();
  };
  switchOrientation(orientation) {
    switch (orientation) {
      case 3:
        return (
          <ClickableImage
            image={this.state.image.url}
            orientation={orientation}
            alt='img'
            width='550'
            height='300'
            style={{ transform: 'rotate(180deg)' }}
            click={this.handleOpen}
          />
        );
      case 6:
        return (
          <ClickableImage
            image={this.state.image.url}
            orientation={orientation}
            alt='img'
            width='270'
            height='240'
            style={{ transform: 'rotate(90deg)' }}
            click={this.handleOpen}
          />
        );
      default:
        return (
          <ClickableImage
            image={this.state.image.url}
            orientation={orientation}
            alt='img'
            width='400'
            height='200'
            click={this.handleOpen}
          />
        );
    }
  }
  componentDidMount() {
    ImageService.getOneImage(this.props.match.params.id)
      .then(res => {
        this.setState({
          image: res.data
        });
      })
      .catch(err => {
        console.log(err);
      });
  }
  render() {
    const { classes } = this.props;
    return (
      <main className={classes.main}>
        {!this.state.open ? <AppAppBar /> : null}
        <Paper className={classes.paper}>
          {this.switchOrientation(this.state.image.orientation)}
          <br />
          <Typography variant='body1' color='primary' gutterBottom>
            Date: {dateFormat(this.state.image.date, 'yyyy/mm/dd h:MM:ss TT')}
          </Typography>
          <Typography variant='body1' color='primary' gutterBottom>
            Tags: {this.state.image.tags}
          </Typography>
          <Typography variant='body1' color='primary' gutterBottom>
            Location:
            <IconButton
              className={classes.button}
              aria-label='Map'
              onClick={this.goToMap}
            >
              <PlaceIcon />
            </IconButton>
          </Typography>

          <Button
            variant='contained'
            color='primary'
            className={classes.return}
            onClick={this.return}
          >
            Return
          </Button>
        </Paper>
      </main>
    );
  }
}
export default withStyles(styles)(ImageDetails);
