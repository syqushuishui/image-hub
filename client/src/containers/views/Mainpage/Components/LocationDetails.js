import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import 'react-datepicker/dist/react-datepicker.css';
// Material UI components
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
// Project components
import AppAppBar from '../../../../components/Navigation/TopAppBar/AppAppBar';
import Map from '../../../../components/Miscellaneous/Map';
import ImageService from '../../../../services/ImageService';

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 800,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex'
  },
  return: {
    display: 'flex',
    marginTop: theme.spacing.unit * 8,
    margin: 'auto'
  },
  text: {
    marginTop: theme.spacing.unit * 4
  }
});

class LocationDetails extends Component {
  state = {
    lat: null,
    lng: null
  };
  return = () => {
    this.props.history.goBack();
  };
  componentDidMount() {
    console.log(this.props.match.params);

    ImageService.getOneImage(this.props.match.params.id)
      .then(res => {
        this.setState({
          lat: res.data.latitude,
          lng: res.data.longitude
        });
        console.log(res);
        this.forceUpdate();
      })
      .catch(err => console.log(err));
  }

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <main className={classes.main}>
          <AppAppBar />
          <Typography variant='subtitle2' className={classes.text}>
            The location of the image on the map
          </Typography>

          <Map isMarkerShown center={this.state} marker={this.state} />
          <Typography variant='body1'>
            Latitude: {this.state.lat}
            <br />
            Longitude: {this.state.lng}
          </Typography>

          <Button
            variant='contained'
            color='primary'
            className={classes.return}
            onClick={this.return}
          >
            Return
          </Button>
        </main>
      </div>
    );
  }
}

export default withStyles(styles)(LocationDetails);
