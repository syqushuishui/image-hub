import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import ReactPaginate from 'react-paginate';
// Material UI components
import Paper from '@material-ui/core/Paper';
// Project components
import AppAppBar from '../../../components/Navigation/TopAppBar/AppAppBar';
import TitlebarGridList from '../../../components/Miscellaneous/TitlebarGridList';
import ImageService from '../../../services/ImageService';

const styles = theme => ({
  main: {
    display: 'flex',
    flexDirection: 'column',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up('md')]: {
      alignItems: 'center'
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`,
    [theme.breakpoints.up('md')]: {
      alignItems: 'center'
    }
  }
});

class Mainpage extends Component {
  state = {
    tile: [],
    limit: 8,
    offset: 0,
    count: 0
  };
  loadDataFromServer = () => {
    ImageService.getAllImages(
      localStorage.getItem('userId'),
      this.state.limit,
      this.state.offset
    )
      .then(res => {
        this.setState({
          tile: res.data.result,
          count: res.data.count
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  handlePageClick = data => {
    let selected = data.selected;
    let offset = Math.ceil(selected * this.state.limit);

    this.setState({ offset: offset }, () => {
      this.loadDataFromServer();
    });
  };

  componentDidMount() {
    this.loadDataFromServer();
  }
  render() {
    const { classes } = this.props;
    return (
      <main className={classes.main}>
        <AppAppBar />
        <Paper className={classes.paper}>
          {this.state.tile.length === 0 ? (
            <TitlebarGridList
              tileData={this.state.tile}
              title='You have no image registered'
            />
          ) : (
            <Fragment>
              <TitlebarGridList
                tileData={this.state.tile}
                title='All your registered images'
              />
              <ReactPaginate
                previousLabel={'<'}
                nextLabel={'>'}
                breakLabel={'...'}
                breakClassName={'break-me'}
                pageCount={Math.ceil(this.state.count / this.state.limit)}
                marginPagesDisplayed={2}
                pageRangeDisplayed={5}
                onPageChange={this.handlePageClick}
                containerClassName={'pagination'}
                activeClassName={'active'}
              />
            </Fragment>
          )}
        </Paper>
      </main>
    );
  }
}

export default withStyles(styles)(Mainpage);
