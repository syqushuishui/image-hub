import React, { Component } from 'react';
// Material UI components
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import Avatar from '@material-ui/core/Avatar';
// Project components
import AppAppBar from '../../../components/Navigation/TopAppBar/AppAppBar';
import UserService from '../../../services/UserService';
import UserDefault from '../../../assets/User.jpg';
import Map from '../../../components/Miscellaneous/Map';
import URLS from '../../../URLS';

const styles = {
  root: {
    maxWidth: 1000,
    margin: 'auto',
    padding: 10,
    paddingTop: 20,
    paddingBottom: 100,
    display: 'grid',
    gridTemplateColumns: '3fr 1fr',
    gridGap: '16px',

    '@media only screen and (max-width: 800px)': {
      gridTemplateColumns: '1fr'
    }
  },
  content: {
    '@media only screen and (max-width: 800px)': {
      order: 1
    }
  },
  avatar: {
    position: 'absolute',
    top: '-70px',
    left: 0,
    right: 0,
    margin: 'auto',
    width: 129,
    height: 129,
    cursor: 'pointer'
  },
  details: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    marginTop: 80,
    border: '1px solid rgba(0,0,0,0.1)',
    maxHeight: 140,
    padding: '80px 12px 12px 12px',
    position: 'relative',
    '@media only screen and (max-width: 800px)': {
      order: 0
    }
  },
  gutter: {
    marginTop: 15
  }
};

class Profile extends Component {
  state = {
    user: []
  };
  componentDidMount() {
    this.fetchUser();
  }
  fetchUser = () => {
    UserService.getUser()
      .then(res => {
        this.setState({ user: res.data });
      })
      .catch(err => console.log(err));
  };
  logout = () => {
    UserService.logOut();
    this.props.history.push(URLS.landing);
  };
  render() {
    const { classes } = this.props;
    return (
      <main>
        <AppAppBar />
        <div className={classes.root}>
          <div className={classes.content}>
            <Typography variant='h5' gutterBottom>
              Your images
            </Typography>
            <Map isMarkerShown />
            <Divider />
          </div>
          <div className={classes.details} dir='column'>
            <Avatar className={classes.avatar} src={UserDefault} />
            <Typography className={classes.gutter} variant='body1'>
              Email: {this.state.user.email}
            </Typography>
            <Typography className={classes.gutter} variant='body1'>
              Username: {this.state.user.username}
            </Typography>
            <Button
              className={classes.gutter}
              variant='contained'
              color='secondary'
              onClick={this.logout}
            >
              Log out
            </Button>
          </div>
        </div>
      </main>
    );
  }
}

export default withStyles(styles)(Profile);
