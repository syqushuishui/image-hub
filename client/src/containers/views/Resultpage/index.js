import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import ReactPaginate from 'react-paginate';
// Material UI components
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
// Project components
import AppAppBar from '../../../components/Navigation/TopAppBar/AppAppBar';
import TitlebarGridList from '../../../components/Miscellaneous/TitlebarGridList';
import ImageService from '../../../services/ImageService';

const styles = theme => ({
  main: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  }
});

class Resultpage extends Component {
  state = {
    tile: [],
    limit: 8,
    offset: 0,
    count: 0
  };

  handlePageClick = data => {
    let selected = data.selected;
    let offset = Math.ceil(selected * this.state.limit);

    this.setState({ offset: offset }, () => {
      this.loadResultFromServer();
    });
  };
  loadResultFromServer = () => {
    ImageService.getSearchImages(
      localStorage.getItem('userId'),
      this.state.limit,
      this.state.offset,
      this.props.location.data[0].fromDate,
      this.props.location.data[0].toDate,
      this.props.location.data[0].tags
    )
      .then(res => {
        this.setState({
          tile: res.data.result,
          count: res.data.count
        });
      })
      .catch(err => {
        console.log(err);
      });
  };

  componentDidMount() {
    this.loadResultFromServer();
  }
  render() {
    const { classes } = this.props;
    return (
      <main className={classes.main}>
        <AppAppBar />
        <Paper className={classes.paper}>
          {this.state.tile === null ? (
            <Typography component='h1' variant='subtitile1'>
              No images found
            </Typography>
          ) : (
            <TitlebarGridList
              tileData={this.state.tile}
              title='Results of the search'
            />
          )}
          <ReactPaginate
            previousLabel={'<'}
            nextLabel={'>'}
            breakLabel={'...'}
            breakClassName={'break-me'}
            pageCount={Math.ceil(this.state.count / this.state.limit)}
            marginPagesDisplayed={2}
            pageRangeDisplayed={5}
            onPageChange={this.handlePageClick}
            containerClassName={'pagination'}
            activeClassName={'active'}
          />
        </Paper>
      </main>
    );
  }
}

export default withStyles(styles)(Resultpage);
