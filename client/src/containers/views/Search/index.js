import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
// Material UI components
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Avatar from '@material-ui/core/Avatar';
// Icons
import SearchIcon from '@material-ui/icons/SearchOutlined';
// Project components
import AppAppBar from '../../../components/Navigation/TopAppBar/AppAppBar';
import URLS from '../../../URLS';

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme
      .spacing.unit * 3}px`
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit * 3
  },
  avatar: {
    margin: theme.spacing.unit
  },
  submit: {
    display: 'flex',
    marginTop: theme.spacing.unit * 8,
    margin: 'auto'
  },
  row: {
    [theme.breakpoints.down('sm')]: {
      display: 'flex',
      flexDirection: 'column'
    }
  },
  date: {
    borderRadius: '5px',
    border: '1px solid lightgray',
    height: '40px',
    width: '100px'
  },
  textfield: {
    width: '330px'
  },
  label: {
    marginRight: '10px'
  }
});

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fromDate: new Date(),
      toDate: new Date(),
      tags: '',
      result: []
    };
    this.handleFromDate = this.handleFromDate.bind(this);
    this.handleToDate = this.handleToDate.bind(this);
  }
  handleFromDate(date) {
    this.setState({
      fromDate: date
    });
  }
  handleToDate(date) {
    this.setState({
      toDate: date
    });
  }
  submit = event => {
    event.preventDefault();
    this.props.history.push({
      pathname: URLS.result,
      data: [
        {
          fromDate: this.state.fromDate,
          toDate: this.state.toDate,
          tags: this.state.tags
        }
      ]
    });
  };
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <main className={classes.main}>
          <AppAppBar />
          <Paper className={classes.paper}>
            <Avatar className={classes.avatar}>
              <SearchIcon />
            </Avatar>
            <Typography component='h1' variant='subtitile1'>
              Search
            </Typography>
            <br />
            <div className={classes.row}>
              <InputLabel className={classes.label} htmlFor='from-date'>
                From-date
              </InputLabel>

              <DatePicker
                className={classes.date}
                selected={this.state.fromDate}
                onChange={this.handleFromDate}
                todayButton={'Today'}
              />

              <InputLabel className={classes.label} htmlFor='to-date'>
                To-date
              </InputLabel>
              <DatePicker
                className={classes.date}
                selected={this.state.toDate}
                onChange={this.handleToDate}
                todayButton={'Today'}
              />
            </div>
            <br />
            <div className={classes.row}>
              <InputLabel className={classes.label} htmlFor='tag'>
                Tag
              </InputLabel>
              <TextField
                className={classes.textfield}
                id='tags'
                variant='outlined'
                value={this.state.tags}
                onChange={event => {
                  this.setState({ tags: event.target.value });
                }}
              />
            </div>
            <Button
              type='submit'
              variant='contained'
              color='primary'
              className={classes.submit}
              onClick={this.submit}
            >
              Search
            </Button>
          </Paper>
        </main>
      </div>
    );
  }
}

export default withStyles(styles)(Search);
