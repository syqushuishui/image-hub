import axios from 'axios';
export default class ImageService {
  static getMetadata() {
    return axios.get('/imagedata');
  }

  static downloadImage(url) {
    return axios.post('/download', {
      url: url
    });
  }
  static getTags(url) {
    return axios.post('/tags', {
      url: url
    });
  }

  static registerImage(
    url,
    date,
    tags,
    latitude,
    longitude,
    orientation,
    userId
  ) {
    return axios.post('/image', {
      url: url,
      date: date,
      tags: tags,
      latitude: latitude,
      longitude: longitude,
      orientation: orientation,
      userId: userId
    });
  }

  static getAllImages(userId, limit, offset) {
    return axios.post('/images', {
      userId: userId,
      limit: limit,
      offset: offset
    });
  }

  static getOneImage(id) {
    return axios.get('/image/' + id);
  }

  static getSearchImages(userId, limit, offset, startDate, endDate, tags) {
    return axios.post('/search', {
      userId: userId,
      limit: limit,
      offset: offset,
      startDate: startDate,
      endDate: endDate,
      tags: tags
    });
  }
}
