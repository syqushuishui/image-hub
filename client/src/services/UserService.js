import axios from 'axios';
import decode from 'jwt-decode';

class User {
  username;
  email;
  password;
}

// Add authorization header to the request
function auth() {
  let token = localStorage.getItem('authToken');
  let Authorization = '';
  if (token) Authorization = 'Bearer ' + token;
  return {
    headers: {
      Authorization
    }
  };
}

export default class UserService {
  static getUser() {
    return axios.get('/user', auth());
  }
  static isAuthenticated() {
    const token = localStorage.getItem('authToken');
    if (token === 'undefined') {
      return false;
    }
    try {
      const { exp } = decode(token); //seconds
      if (exp < new Date().getTime() / 1000) {
        // getTime method is the number of milliseconds
        return false;
      }
    } catch (e) {
      return false;
    }
    return true;
  }

  static createUser(username, email, password) {
    let user = new User();
    user.username = username;
    user.email = email;
    user.password = password;
    return axios.post('/user', user);
  }
  static loginUser(email, password) {
    let user = new User();
    user.email = email;
    user.password = password;
    return axios.post('/login', user);
  }

  static logOut() {
    window.localStorage.clear();
  }

  static resetPassword(email) {
    return axios.post('/reset', {
      email: email
    });
  }
}
