const bcrypt = require('bcrypt');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

const nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');

const User = require('../models/user');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

const transporter = nodemailer.createTransport(
  sendgridTransport({
    auth: {
      api_key: `${process.env.SEND_GRID_API_KEY}`
    }
  })
);

// Get User => GET
exports.getUser = (req, res) => {
  //console.log(req.headers.authorization);
  jwt.verify(
    req.headers.authorization.split(' ')[1],
    'supersupersecretsecret',
    (err, decoded) => {
      /*if (err) {
        console.log('err');
        return res.status(401).json(err);
      }*/
      User.findOne({ where: { id: decoded.userId } })
        .then(user => {
          res.status(200).json(user);
        })
        .catch(err => {
          res.status(401).json(err);
        });
    }
  );
};

// Create a user => POST
exports.createUser = (req, res) => {
  User.findAll({
    where: {
      [Op.or]: [{ email: req.body.email }, { username: req.body.username }]
    }
  }).then(user => {
    if (user.length == 1) {
      return res.status(401).json({
        message: 'This email address or username is already registered'
      });
    } else {
      bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash(req.body.password, salt, function(err, hash) {
          User.create({
            username: req.body.username,
            email: req.body.email,
            hash: hash,
            salt: salt
          })
            .then(result => {
              res.status(201).json(result);
              transporter.sendMail({
                to: req.body.email,
                from: 'noreply@imagehub.com',
                subject: 'Welcome to Image hub',
                html: `
                <h2>You have registered successfully.</h2>
                <p>Now you can start the service now.</p>
                `
              });
            })
            .catch(err => {
              res.status(500).json({ message: err.message });
            });
        });
      });
    }
  });
};

// Login
exports.loginUser = (req, res) => {
  // Check if user exists
  User.findAll({ where: { email: req.body.email } })
    .then(user => {
      if (user.length < 1) {
        return res.status(402).json({
          message: 'User do not exist'
        });
      }
      // Check password
      bcrypt.compare(req.body.password, user[0].hash, (err, result) => {
        if (result) {
          const token = jwt.sign(
            {
              email: user[0].email,
              userId: user[0].id.toString()
            },
            'supersupersecretsecret',
            { expiresIn: '1h' }
          );
          return res.status(200).json({
            token: token,
            userId: user[0].id.toString()
          });
        } else {
          res.status(401).json({
            message: 'Incorrect email or password'
          });
        }
      });
    })
    .catch(error => {
      res.status(500).json({ message: error.message });
    });
};

// Reset Password
exports.resetPassword = (req, res) => {
  crypto.randomBytes(32, (err, buffer) => {
    if (err) {
      console.log(err);
    }
    const token = buffer.toString('hex');
    User.findOne({ where: { email: req.body.email } })
      .then(user => {
        if (!user) {
          return res.json({ errorMessage: 'No account with that email found' });
        }
        user.resetToken = token;
        user.resetTokenExpiration = Date.now() + 3600000; // 1 hour later
        return user.save();
      })
      .then(result => {
        transporter.sendMail({
          to: req.body.email,
          from: 'noreply@imagehub.com',
          subject: 'Password Reset',
          html: `
          <h2>You requested a password reset</h2>
          <p>Click this <a href='http:localhost:3001/reset/${token}'>link</a> to set a new password.</p>
          `
        });
        res.status(201).json(result);
      })
      .catch(err => {
        console.log(err);
      });
  });
};

exports.getNewPassword = (req, res) => {
  const token = req.params.token;
  User.findOne({
    where: { resetToken: token, resetTokenExpiration: { $gt: Date.now() } }
  })
    .then(user => {})
    .catch(err => {
      console.log(err);
    });
};
// Update password
exports.updateUser = (req, res) => {};

exports.getExifData = (req, res) => {
  var ExifImage = require('exif').ExifImage;

  try {
    new ExifImage(
      {
        image:
          'C:/Users/Shanshan Qu/Documents/Workplace/image-hub/client/src/assets/photo-1553288856-8b0781fcb208.jpg'
      },
      function(error, exifData) {
        if (error) console.log('Error: ' + error.message);
        else console.log(exifData); // Do something with your data!
      }
    );
  } catch (error) {
    console.log('Error: ' + error.message);
  }
};
