var ExifImage = require('exif').ExifImage;
const Image = require('../models/image');
const Fs = require('fs');
const Sequelize = require('sequelize');
const Op = Sequelize.Op;
var request = require('request');

exports.getExifData = (req, res) => {
  try {
    console.log('Getting image data');
    new ExifImage({ image: 'src/temp/test.jpg' }, function(error, exifData) {
      if (error) {
        console.log('Error: ' + error.message);
        res.status(401).json({
          message: error.message
        });
      } else {
        var json = res.status(200).json(exifData);
        return json;
      }
    });
  } catch (error) {
    res.status(401).json({
      message: 'Error'
    });
  }
};

exports.postImage = (req, res) => {
  Image.create({
    url: req.body.url,
    date: req.body.date,
    tags: req.body.tags,
    latitude: req.body.latitude,
    longitude: req.body.longitude,
    orientation: req.body.orientation,
    userId: req.body.userId
  })
    .then(result => {
      res.status(201).json(result);
      //delete the temp image in the temp folder
      var filePath = 'src/temp/test.jpg';
      Fs.unlinkSync(filePath);
    })
    .catch(err => {
      res.status(500).json({ message: err.message });
    });
};

exports.downloadImage = async (req, res) => {
  const Path = require('path');
  const Axios = require('axios');

  const url = req.body.url;
  const path = Path.resolve('src/temp', 'test.jpg');
  const writer = Fs.createWriteStream(path);

  const response = await Axios({
    url,
    method: 'GET',
    responseType: 'stream'
  });

  response.data.pipe(writer);
  var promise = new Promise((resolve, reject) => {
    writer.on('finish', resolve);
    writer.on('error', reject);
  });
  promise.then(function(value) {
    res.status(200).json('success');
  });
  return promise;
};

exports.getAllImages = (req, res) => {
  var limit = req.body.limit;
  var offset = req.body.offset;
  Image.findAndCountAll({ where: { userId: req.body.userId } })
    .then(data => {
      let pages = Math.ceil(data.count / limit);

      Image.findAll({
        limit: limit,
        offset: offset,
        where: { userId: req.body.userId }
      }).then(images => {
        res
          .status(200)
          .json({ result: images, count: data.count, pages: pages });
      });
    })
    .catch(error => {
      res.status(500).json({ message: error.message });
    });
};

exports.getOneImage = (req, res) => {
  Image.findOne({ where: { id: req.params.id } })
    .then(image => {
      return res.status(200).json(image);
    })
    .catch(error => {
      res.status(500).json({ message: error.message });
    });
};

exports.getSearchImages = (req, res) => {
  var limit = req.body.limit;
  var offset = req.body.offset;
  Image.findAndCountAll({
    where: {
      userId: req.body.userId,
      date: {
        [Op.lt]: req.body.endDate,
        [Op.gt]: req.body.startDate
      },
      tags: {
        [Op.like]: `%${req.body.tags}%`
      }
    }
  })
    .then(data => {
      let pages = Math.ceil(data.count / limit);

      Image.findAll({
        limit: limit,
        offset: offset,
        where: {
          userId: req.body.userId,
          date: {
            [Op.lt]: req.body.endDate,
            [Op.gt]: req.body.startDate
          },
          tags: {
            [Op.like]: `%${req.body.tags}%`
          }
        }
      }).then(images => {
        res
          .status(200)
          .json({ result: images, count: data.count, pages: pages });
      });
    })
    .catch(error => {
      res.status(500).json({ message: error.message });
    });
};

exports.getTags = (req, res) => {
  (apiKey = 'acc_8323cb798644355'),
    (apiSecret = '6d240d62788f4ac6b5426150cd4fc597'),
    (imageUrl = req.body.url);

  request
    .get(
      'https://api.imagga.com/v2/tags?image_url=' +
        encodeURIComponent(imageUrl),
      function(error, response, body) {
        res.status(200).json({ body });
      }
    )
    .auth(apiKey, apiSecret, true);
};
