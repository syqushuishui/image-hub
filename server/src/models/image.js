const Sequelize = require('sequelize');
const sequelize = require('../util/database');

const Image = sequelize.define('image', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true
  },
  url: {
    type: Sequelize.STRING,
    allowNull: false
  },
  date: Sequelize.DATE,
  tags: Sequelize.STRING,
  latitude: Sequelize.DOUBLE,
  longitude: Sequelize.DOUBLE,
  orientation: Sequelize.INTEGER,
  country: Sequelize.STRING,
  county: Sequelize.STRING,
  city: Sequelize.STRING
});

module.exports = Image;
