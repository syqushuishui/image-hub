const express = require('express');

const authController = require('../controllers/auth');
const router = express.Router();

router.get('/user', authController.getUser);
router.post('/user', authController.createUser);
router.put('/user/:id', authController.updateUser);
router.post('/login', authController.loginUser);
router.get('/image', authController.getExifData);
//router.post('/reset', authController.resetPassword);

module.exports = router;
