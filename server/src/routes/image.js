const express = require('express');

const imageController = require('../controllers/image');
const router = express.Router();

router.get('/imagedata', imageController.getExifData);
router.post('/image', imageController.postImage);
router.post('/download', imageController.downloadImage);
router.post('/images', imageController.getAllImages);
router.get('/image/:id', imageController.getOneImage);
router.post('/search', imageController.getSearchImages);
router.post('/tags', imageController.getTags);
module.exports = router;
