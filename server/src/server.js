const path = require('path');
const express = require('express');
const userRoutes = require('./routes/auth');
const imageRoutes = require('./routes/image');
const public_path = path.join(__dirname, '/../../client/public');
const sequelize = require('./util/database');

const User = require('./models/user');
const Image = require('./models/image');

let app = express();
app.use(express.static(public_path));
app.use(express.json()); // For parsing application/json

// Relations
Image.belongsTo(User);
// Database synchronization
sequelize
  .sync(/*{ force: true }*/)
  .then(result => {
    console.log(result);
  })
  .catch(err => {
    console.log(err);
  });

app.use(userRoutes);
app.use(imageRoutes);

let listen = new Promise((resolve, reject) => {
  app.listen(3001, error => {
    if (error) reject(error.message);
    console.log('Server started');
    resolve();
  });
});
